//
//  AC_ChallengeTests.swift
//  AC_ChallengeTests
//
//  Created by Vinícius  Gontijo on 02/06/2018.
//  Copyright © 2018 Vinícius  Gontijo. All rights reserved.
//

import XCTest
@testable import AC_Challenge


class AC_ChallengeTests: XCTestCase {

    var searchVC: SearchLocationViewController?
    var mapViewVC: MapViewController?
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        self.searchVC = SearchLocationViewController()
        self.mapViewVC = MapViewController()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        self.searchVC = nil
        self.mapViewVC = nil
    }
    
    
    func testSearchViewProps() {
        self.searchVC?.selectedPlaceIndex = 1
        
        XCTAssertNotNil(self.searchVC?.searchResults)
        XCTAssertGreaterThan(self.searchVC!.selectedPlaceIndex!, 0, "must not be a negative number")
    }
    
    func testMapViewProps(){
        self.mapViewVC?.selectedPlaceIndex = 1
        
        XCTAssertNotNil(self.mapViewVC?.places)
        XCTAssertGreaterThan(self.mapViewVC!.selectedPlaceIndex!, 0, "must not be a negative number")
    }
    
    func testPlaceInitIsCorretly() {
        let place = Place(placeID: "0", name: "Test", latitude: 0, longitude: 0)
    
        XCTAssertNotNil(place)
    }
    
    func testPlacePropertyIsCorretly() {
        let place = Place(placeID: "1", name: "Lagoa Formosa", latitude: 0, longitude: 0)
        
        XCTAssertEqual(place.placeID, "1", "placeid not equal")
        XCTAssertEqual(place.name, "Lagoa Formosa", "name not equal")
        XCTAssertEqual(place.placeLat, 0, "lat equal")
        XCTAssertEqual(place.placeLong, 0, "long equal")
    }
    
    func testPlacesResultsArrayIsCorretly(){
        
        let p1 = Place(placeID: "0", name: "Test1", latitude: 0, longitude: 0)
        let p2 = Place(placeID: "1", name: "Test2", latitude: 0, longitude: 0)
        let mockPlaces = [p1,p2]
        
        self.mapViewVC?.places = mockPlaces
        
        XCTAssertNotNil(self.mapViewVC?.places)
        XCTAssertEqual(self.mapViewVC?.places[0].placeID, mockPlaces[0].placeID, "Places not equal")
    }
}
