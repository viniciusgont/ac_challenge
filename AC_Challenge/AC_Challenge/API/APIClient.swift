//
//  APIClient.swift
//  DesafioiOSConcrete
//
//  Created by Vinícius  Gontijo on 20/01/2018.
//  Copyright © 2018 Vinícius  Gontijo. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

enum Router: URLRequestConvertible{
    static let baseURLAPI = "https://maps.googleapis.com/maps/api"
    
    case SearchLocation(address: String)
    
    var path: (method: HTTPMethod, url: String){
        switch self {
        case .SearchLocation:
            return (.get, "/geocode/json" )
        }
    }
    
    ///Build URLRequest for each API method
    func asURLRequest() throws -> URLRequest {
        let url = URL(string: Router.baseURLAPI)!
        var urlRequest = URLRequest(url: url.appendingPathComponent(path.url))
        urlRequest.httpMethod = path.method.rawValue
        
        switch self {
        case .SearchLocation(let address):
            return try Alamofire.URLEncoding.default.encode(urlRequest, with: ["address": address, "key" : Helper.API_KEY])
            
        }
    }
}

class APIClient {
    
    static let domain = "com.googleapis.maps"
    
    enum errorCode: Int {
        case nonRetryCode = 0
        case retryCode = 1
    }
    
    //MARK: Requests from Controllers
    class func searchPlace(address: String,
                           success: @escaping ([Place]) -> Void,
                           failure: @escaping (NSError) -> Void) -> Request{
        
        return request(urlRequestConvertible:
            Router.SearchLocation(address: address), JSONKey: "results", success: success, failure: failure)
        
    }
}

//MARK: Request helpers
extension APIClient{
    
    ///JSON Result
    private class func request<T:Mappable>(urlRequestConvertible:URLRequestConvertible,JSONKey: String,
                                           success:@escaping (T) -> Void,
                                           failure:@escaping (NSError) -> Void) -> Request{
        return self.request(urlRequestConvertible: urlRequestConvertible, type: T.self, JSONKey: JSONKey,
                            success: { (any) -> Void in
                                if let object = any as? T{
                                    success(object)
                                }else{
                                    failure(NSError(domain: self.domain, code: APIClient.errorCode.nonRetryCode.rawValue, userInfo: [NSLocalizedDescriptionKey: "Error on parsing: \(any)"]))
                                }
        },
                            failure: failure)
    }
    
    ///JSON Result with array
    private class func request<T:Mappable>(urlRequestConvertible:URLRequestConvertible, JSONKey: String,
                                           success:@escaping ([T]) -> Void,
                                           failure:@escaping (NSError) -> Void) -> Request{
        return self.request(urlRequestConvertible: urlRequestConvertible, isArray: true, type: T.self, JSONKey: JSONKey, success: { (any) -> Void in
            if let objects = any as? [T]{
                success(objects)
            }else{
                failure(NSError(domain: self.domain, code: APIClient.errorCode.nonRetryCode.rawValue, userInfo: [NSLocalizedDescriptionKey: "Error on parsing: \(any)"]))
            }
        }, failure: failure)
    }
    
    ///Get the data and the result mappable
    private class func request<T:Mappable>(urlRequestConvertible:URLRequestConvertible, isArray:Bool = false, type:T.Type, JSONKey: String,
                                           success:@escaping (Any) -> Void,
                                           failure:@escaping (NSError) -> Void) -> Request{
        return Alamofire.request(urlRequestConvertible)
            .validate()
            .responseJSON(options: JSONSerialization.ReadingOptions.mutableContainers) { (response) -> Void in
                
                debugPrint(response)
                
                switch response.result {
                case .success(let resultJSON):
                    
                    let mappableObject:Any?
                    if isArray{
                        if !JSONKey.isEmpty{
                            //Search for json key
                            let data = resultJSON as! NSDictionary
                            mappableObject = Mapper<T>().mapArray(JSONArray: data[JSONKey] as! [[String : Any]])
                        } else{
                            mappableObject = Mapper<T>().mapArray(JSONObject: resultJSON)
                        }
                    }else{
                        mappableObject = Mapper<T>().map(JSONObject: resultJSON)
                    }
                    
                    if let mappableObject = mappableObject{
                        success(mappableObject)
                    }else{
                        if response.response != nil{
                            failure(NSError(domain: self.domain, code: APIClient.errorCode.nonRetryCode.rawValue, userInfo: [NSLocalizedDescriptionKey: "Error on parsing: \(resultJSON)"]))
                        }
                    }
                    
                case .failure(let error):
                    if let alamoResponse = response.response{
                        if alamoResponse.statusCode != 404{
                            failure(NSError(domain: error._domain, code: alamoResponse.statusCode, userInfo: [NSLocalizedDescriptionKey: error.localizedDescription]))
                        }
                    } else {
                        failure(error as NSError)
                    }
                }
        }
    }
}
