//
//  CoreDataController.swift
//  AC_Challenge
//
//  Created by Vinícius  Gontijo on 04/06/2018.
//  Copyright © 2018 Vinícius  Gontijo. All rights reserved.
//

import CoreData
import UIKit
import GoogleMaps

class CoreDataController {
    
    static let placeEntityName = "PlaceEntity"
    
    static let placeIDAttr = "placeID"
    static let placeNameAttr = "name"
    static let placeLatAttr = "latitude"
    static let placeLongAttr = "longitude"
    
    ///Save place using CoreData
    class func savePlaceContext (place: Place) -> Bool {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{
            return false
        }
        
        if #available(iOS 10.0, *) {
            let managedContext = appDelegate.persistentContainer.viewContext
            
            let entity = NSEntityDescription.entity(forEntityName: placeEntityName, in: managedContext)
            let newPlace = NSManagedObject(entity: entity!, insertInto: managedContext)
            
            newPlace.setValue(place.placeID, forKey: placeIDAttr)
            newPlace.setValue(place.name, forKey: placeNameAttr)
            newPlace.setValue(place.placeLat, forKey: placeLatAttr)
            newPlace.setValue(place.placeLong, forKey: placeLongAttr)
            
            //let sqlurl = appDelegate.persistentContainer.persistentStoreCoordinator.persistentStores.first?.url
            //print(sqlurl)
            
            if managedContext.hasChanges {
                do {
                    try managedContext.save()
                    return true
                } catch {
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                    }
                }
            } else {
                // iOS 9
                let managedContext = appDelegate.managedObjectContext
                let entity =  NSEntityDescription.entity(forEntityName: placeEntityName, in:managedContext)
                let newPlace = NSManagedObject(entity: entity!, insertInto: managedContext)
            
                newPlace.setValue(place.placeID, forKey: placeIDAttr)
                newPlace.setValue(place.name, forKey: placeNameAttr)
                newPlace.setValue(place.placeLat, forKey: placeLatAttr)
                newPlace.setValue(place.placeLong, forKey: placeLongAttr)
                
                if appDelegate.managedObjectContext.hasChanges {
                    do {
                        try appDelegate.managedObjectContext.save()
                        return true
                    } catch {
                        let nserror = error as NSError
                        NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                        abort()
                    }
                }
            }
        
        return false
    }
    
    ///Delete place from CoreData
    class func deletePlaceFromContext(placeID: String) -> Bool {
    
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{
            return false
        }
        
         if #available(iOS 10.0, *) {
            let managedContext = appDelegate.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: placeEntityName)
            
            do{
                var places = try managedContext.fetch(fetchRequest)
                
                for (index, data) in places.enumerated(){
                    let placeId = data.value(forKey: placeIDAttr) as! String
                    
                    if placeId == placeID{
                        //Delete place from context
                        managedContext.delete(places[index] as NSManagedObject)
                        places.remove(at: index)
                    }
                }
                
                do {
                    try managedContext.save()
                    return true
                } catch {
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            } catch let error as NSError{
                print("Error \(error)")
            }
            
         } else{
            //iOS9
            let managedContext = appDelegate.managedObjectContext
            let entity =  NSEntityDescription.entity(forEntityName: placeEntityName, in:managedContext)
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.entity = entity
            
            do{
                var places = try managedContext.fetch(fetchRequest)
                
                for (index, data) in places.enumerated(){
                    let placeId = (data as AnyObject).value(forKey: placeIDAttr) as! String
                    
                    if placeId == placeID{
                        //Delete place from context
                        managedContext.delete(places[index] as! NSManagedObject)
                        places.remove(at: index)
                    }
                }
                
                do {
                    try managedContext.save()
                    return true
                } catch {
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            } catch let error as NSError{
                print("Error \(error)")
            }
        }
        
        return false
    }
    
    ///Find a place CoreData
    class func searchPlaceFromContext(placeID: String) -> Place? {
    
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else{
            return nil
        }
        
        if #available(iOS 10.0, *) {
            let managedContext = appDelegate.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: placeEntityName)
            
            do{
                let places = try managedContext.fetch(fetchRequest)
                
                for data in places{
                    let placeId = data.value(forKey: placeIDAttr) as! String
                    
                    if placeId == placeID{
                        //Get place from context
                        let name = data.value(forKey: placeNameAttr) as! String
                        let lat = data.value(forKey: placeLatAttr) as! Double
                        let long = data.value(forKey: placeLongAttr) as! Double
                        
                        return Place(placeID: placeId, name: name, latitude: lat, longitude: long)
                    }
                }
            } catch let error as NSError{
                print("Error \(error)")
            }
            
        } else{
            //iOS9
            let managedContext = appDelegate.managedObjectContext
            let entity =  NSEntityDescription.entity(forEntityName: placeEntityName, in:managedContext)
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            fetchRequest.entity = entity
            
            do{
                let places = try managedContext.fetch(fetchRequest)
                for data in places{
                    let placeId = (data as AnyObject).value(forKey: placeIDAttr) as! String
                    
                    if placeId == placeID{
                        //Get place from context
                        let name = (data as AnyObject).value(forKey: placeNameAttr) as! String
                        let lat = (data as AnyObject).value(forKey: placeLatAttr) as! Double
                        let long = (data as AnyObject).value(forKey: placeLongAttr) as! Double
                        
                        return Place(placeID: placeId, name: name, latitude: lat, longitude: long)
                    }
                }
            } catch{
                let fetchError = error as NSError
                print(fetchError)
            }
        }
        
        return nil
    }
}
    

