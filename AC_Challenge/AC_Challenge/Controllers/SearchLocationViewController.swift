//
//  ViewController.swift
//  AC_Challenge
//
//  Created by Vinícius  Gontijo on 02/06/2018.
//  Copyright © 2018 Vinícius  Gontijo. All rights reserved.
//

import UIKit
import GoogleMaps

class SearchLocationViewController: BaseViewController, UISearchBarDelegate  {
    
    @IBOutlet weak var searchBarController: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var searchResults = [Place]()
    var selectedPlaceIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpSearchBarController()
        setUpTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //Reset selectedPlaceIndex
        selectedPlaceIndex = nil
    }
    
    func setUpSearchBarController() {
        searchBarController.delegate = self
        searchBarController.placeholder = "Search on Google Maps"
    }
    
    func setUpTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchPlacesFromAPI(searchText)
    }
    
    func searchPlacesFromAPI(_ address: String){
        
        _ = APIClient.searchPlace(address: address, success: {[weak self] (places) in
            
            if let strongSelf = self{
                strongSelf.searchResults.removeAll()
                strongSelf.searchResults.append(contentsOf: places)
                strongSelf.tableView.reloadData()
                
                if strongSelf.searchResults.count == 0{
                    strongSelf.showNoResults()
                } else {
                    strongSelf.hideNoResults()
                }
            }
            
        }) { (error) in
            print(error)
            //Helper.showAlert(viewController: self , title: "Erro", message: error.description)
        }
    }
    

    func showNoResults(){
        
        //If this tag already exists, return 
        if self.view.viewWithTag(10) != nil {
            return
        }
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
        label.center = CGPoint(
            x: searchBarController.bounds.size.width/2,
            y: searchBarController.bounds.size.height + 100)
        label.textAlignment = .center
        label.text = "No Results"
        label.tag = 10
        self.view.addSubview(label)
    }
    
    func hideNoResults(){
        guard let labelViewWithTag = self.view.viewWithTag(10) else {
            return
        }
        labelViewWithTag.removeFromSuperview()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let mapViewController = segue.destination as? MapViewController{
            mapViewController.places = self.searchResults
            mapViewController.selectedPlaceIndex = self.selectedPlaceIndex
        }
    }
}

//MARK: Tableviews
extension SearchLocationViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        //Show only one row in the first section
        if self.searchResults.count > 1 && section == 0{
            return 1
        }
        return self.searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCell", for: indexPath)
        
        if self.searchResults.count > 1 && indexPath.section == 0{
            cell.textLabel?.text = "Display All on Map"
        } else {
            cell.textLabel?.text = self.searchResults[indexPath.row].name
        }
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.searchResults.count > 1{
            return 2
        } else if self.searchResults.count == 0{
            return 0
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 || self.searchResults.count == 1{
            self.selectedPlaceIndex = indexPath.row
        }
        
        performSegue(withIdentifier: "segueSearchToMapView", sender: self)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        //Just to separete sections
        return " "
    }
}
