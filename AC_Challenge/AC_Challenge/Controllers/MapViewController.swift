//
//  MapViewController.swift
//  AC_Challenge
//
//  Created by Vinícius  Gontijo on 03/06/2018.
//  Copyright © 2018 Vinícius  Gontijo. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewController: BaseViewController, GMSMapViewDelegate {
    
    @IBOutlet weak var mapView: GMSMapView!
    
    var places = [Place]()
    var selectedPlaceIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpMapView()
        setNavigationBarButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showAnnotations()
    }
    
    func setUpMapView() {
        mapView.delegate = self
        let camera = GMSCameraPosition.camera(withLatitude: 0, longitude: 0, zoom: 5.0)
        self.mapView.camera = camera;
    }
    
    func setNavigationBarButton() {
        if selectedPlaceIndex != nil {
            //Add Navigationbar Button
            let placeSaved = CoreDataController.searchPlaceFromContext(placeID: places[selectedPlaceIndex!].placeID!)
            
            //Place exists in coredata
            if placeSaved != nil {
                showDeleteButton()
            } else {
                showAddButton()
            }
        } else{
            //Display all
            removeNavigationBar()
        }
    }
    
    func showAddButton() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(savePlaceToCoreData))
    }
    
    func showDeleteButton() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Delete", style: .plain, target: self, action: #selector(deletePlace))
    }
    
    func removeNavigationBar() {
        self.navigationItem.rightBarButtonItem = nil
    }
    
    @objc func savePlaceToCoreData() {
        
        guard let selectedIndex = selectedPlaceIndex else {
            print("No place selected")
            return
        }
        
        if(CoreDataController.savePlaceContext(place: places[selectedIndex])){
            Helper.showAlert(self, title: "Success", message: Helper.itemSaved)
            setNavigationBarButton()
        } else {
            Helper.showAlert(self, title: "Error", message: "Error occured!")
        }
    }
    
    @objc func deletePlace() {
        
        //Display alert for confirmation
        let alert = UIAlertController(title: "Confirm", message: "Are you sure you want to delete this item?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
            self.deletePlaceCoreData()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func deletePlaceCoreData(){
        
        guard let selectedIndex = selectedPlaceIndex else {
            Helper.showAlert(self, title: "Error", message: "Error")
            return
        }
        let deleted = CoreDataController.deletePlaceFromContext(placeID: places[selectedIndex].placeID!)
         
         if (deleted){
            Helper.showAlert(self, title: "Success", message: Helper.itemDeleted)
            setNavigationBarButton()
         } else {
            Helper.showAlert(self, title: "Error", message: "Error")
         }
    }
    
    func showAnnotations(){
        var bounds = GMSCoordinateBounds()
        
        for (index, place) in self.places.enumerated() {
            let marker = GMSMarker()
            marker.position = place.placeLocation!
            marker.title = place.name
            marker.snippet = "\(place.placeLat!, place.placeLong!)"
            marker.map = self.mapView
            
            if self.selectedPlaceIndex != nil && index == self.selectedPlaceIndex{
                self.mapView.camera = GMSCameraPosition.camera(withTarget: place.placeLocation!, zoom: 5)
            } else if(self.selectedPlaceIndex == nil){
                //Save bound to display all on map option
                bounds = bounds.includingCoordinate(place.placeLocation!)
            }
        }
        
        //Display all on map option, show all markers
        if(self.selectedPlaceIndex == nil){
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 50.0,left: 50.0,bottom: 50.0,right: 50.0)))
        }
    }
}
