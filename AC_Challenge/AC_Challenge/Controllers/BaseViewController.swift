//
//  BaseViewController.swift
//  AC_Challenge
//
//  Created by Vinícius  Gontijo on 02/06/2018.
//  Copyright © 2018 Vinícius  Gontijo. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    //Use this class to make a global menu, e.q.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configActions()
        configAppearence()
    }
    
    func configActions(){
        //override this
    }
    
    func configAppearence(){
        //override this
    }
}
