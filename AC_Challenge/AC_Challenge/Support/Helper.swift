//
//  Helper.swift
//  AC_Challenge
//
//  Created by Vinícius  Gontijo on 03/06/2018.
//  Copyright © 2018 Vinícius  Gontijo. All rights reserved.
//

import UIKit
import SVProgressHUD

//MARK: General funcions
class Helper {
    
    //MARK: Alert Views
    class func showAlert(_ viewController: UIViewController, title: String, message: String, block: (() -> Void)? = nil){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let retryAction = UIAlertAction(title: "OK", style: .default, handler: { (_) -> Void in
            block?()
        })
        alertController.addAction(retryAction)
        
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    class func showAlert(_ viewController: UIViewController, title: String, message: String, options: [String], block: @escaping ((_ selectedString: String) -> Void)){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for option in options{
            let optionAction = UIAlertAction(title: option, style: .default, handler: { (alertAction) -> Void in
                block(alertAction.title! )
            })
            alertController.addAction(optionAction)
        }
        
        viewController.present(alertController, animated: true, completion: nil)
    }
}

//MARK: SVProgressHUD
extension Helper{
    class func hudShowWithStatus(_ status: String){
        SVProgressHUD.show(withStatus: status)
    }
    
    class func hudDismiss(){
        SVProgressHUD.dismiss()
    }
    
    class func hudShowToastWithStatus(_ status: String){
        SVProgressHUD.showSuccess(withStatus: status)
    }
}

//MARK: String DB
extension Helper{
    static let API_KEY = "AIzaSyCxUoAzsaEXz9GXPtBdHrwKaDEdTMXMoQY"
    
    static let itemSaved = "Item saved!"
    static let itemDeleted = "Item deleted!"
}

