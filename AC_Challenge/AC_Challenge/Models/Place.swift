//
//  Location.swift
//  AC_Challenge
//
//  Created by Vinícius  Gontijo on 03/06/2018.
//  Copyright © 2018 Vinícius  Gontijo. All rights reserved.
//

import Foundation
import GoogleMaps
import ObjectMapper

class Place: Mappable  {
    
    var placeID: String?
    var name: String?
    var placeLat: Double?
    var placeLong: Double?
    
    var placeLocation:CLLocationCoordinate2D?{
        if let latitude = placeLat, let longitude = placeLong{
            return CLLocationCoordinate2DMake(latitude, longitude)
        }else{
            return nil
        }
    }
    
    init(placeID: String, name: String, latitude: Double, longitude: Double ) {
        self.placeID = placeID
        self.name = name
        self.placeLat = latitude
        self.placeLong = longitude
    }
    
    //MARK: Mappable requirements
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        name <- map["formatted_address"]
        placeID <- map["place_id"]
        placeLat <- map["geometry.location.lat"]
        placeLong <- map["geometry.location.lng"]
    }
}


